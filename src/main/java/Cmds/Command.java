package Cmds;

import java.io.IOException;
import java.util.Map;
import no.uib.inf101.terminal.Context;

public interface Command {

    default void setContext(Context context) {

    }


    default void setCommandContext(Map<String, Command> cmd) {
    }

    /**
     * @param args Arguments given to the shell
     * @return string command
     */
    String run(String[] args);

    /**
     * @return The name of the command
     */
    String getName();

    String getManual();
}
