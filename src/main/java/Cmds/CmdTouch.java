package Cmds;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import no.uib.inf101.terminal.Context;

public class CmdTouch implements Command {

    private Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    ;


    @Override
    public String run(String[] args)  {
        File cwd = this.context.getCwd();
        if (args.length == 0) {
            return "Your file needs a name";
        } else if (args.length == 1) {
            File file = new File(cwd, args[0]);
            try{
            file.createNewFile();}
            catch(IOException e){
                throw new RuntimeException("File not created");
            }
        } else {
            return "Too many arguments";
        }
        return "";
    }


    @Override
    public String getName() {
        return "touch";
    }

    @Override
    public String getManual() {
        return """
                Make an empty file with the given name and extension.
                """;
    }
}
