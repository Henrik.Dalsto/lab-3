package Cmds;

import java.io.File;
import no.uib.inf101.terminal.Context;

public class CmdLs implements Command {

    private Context context;


    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        File cwd = this.context.getCwd();
        String s = "";
        if (args.length == 0) {
            for (File file : cwd.listFiles()) {
                if (file.isHidden()) {
                    continue;
                }
                s += file.getName();
                s += " ";
            }
        } else if (args.length > 1) {
            return "ls: too many arguments";
        } else {
            s = optionalParameters(args[0], cwd);
        }

        return s;
    }

    /**
     * @param arg Argument passed to ls
     * @param cwd the directory we are in
     * @return Returns a string representation of the files in the directory
     */
    public String optionalParameters(String arg, File cwd) {
        String s = "";
        switch (arg) {
            case "-l":
                for (File file : cwd.listFiles()) {
                    if (file.isHidden()) {
                        continue;
                    }
                    s += file.getName();
                    s += "\n";
                }
                break;
            case "-a":
                for (File file : cwd.listFiles()) {
                    if (file.isHidden()) {
                        continue;
                    }
                    s += file.getName();
                    s += " ";
                }
                break;
            case "-la":
                for (File file : cwd.listFiles()) {
                    s += file.getName();
                    s += "\n";
                }
                break;
            default:
                File directory = new File(cwd + "\\" + arg);
                for (File file : directory.listFiles()) {
                    if (file.isHidden()) {
                        continue;
                    }
                    s += file.getName();
                    s += " ";
                }
                break;
        }

        return s;
    }


    @Override
    public String getName() {
        return "ls";
    }

    @Override
    public String getManual() {
        return """
                Lists files in folder. Accepts arguments:
                ls-l    Shows elements separated by newline
                ls-a    Shows hidden files
                ls-la   Show all files separated by newline
                ls path/to/folder   Shows files in folder cwd/path/to/folder""";
    }
}
