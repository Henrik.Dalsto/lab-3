package Cmds;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        StringBuilder builder = new StringBuilder();
        for (String arg : args) {
            builder.append(arg).append(' ');
        }
        return builder.toString();
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return """
                Print args to screen.
                """;
    }
}
