package Cmds;

public class CmdExit implements Command {

    @Override
    public String run(String[] args) {
        System.exit(0);
        return null;
    }

    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public String getManual() {
        return """
                Exit the shell.
                """;
    }
}
