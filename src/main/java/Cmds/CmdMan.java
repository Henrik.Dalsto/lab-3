package Cmds;

import java.util.HashMap;
import java.util.Map;
import no.uib.inf101.terminal.Context;

public class CmdMan implements Command {

    private Map<String, Command> cmds;

    @Override
    public void setCommandContext(Map<String, Command> cmds) {
        this.cmds = cmds;
    }

    @Override
    public String run(String[] args) {
        return cmds.get(args[0]).getManual();
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return """
                Get help
                """;
    }
}
